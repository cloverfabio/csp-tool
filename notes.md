---

### Source Aliases

`electron-webpack` provides a few [source aliases](https://webpack.js.org/configuration/resolve/#resolve-alias), or [path mappings](https://www.typescriptlang.org/docs/handbook/module-resolution.html#path-mapping) as TypeScript would call it, that allow you to import your modules easier.

* `@`: Provides path to corresponding process (`main` or `renderer`)
* `common`: Provides path to common source directory


---