import React from 'react';
import ReactDOM from 'react-dom';

import 'semantic-ui/dist/semantic.min.css';
import '@/styles/index.css';

import App from 'common/components/App';

ReactDOM.render(
    <App />,
    document.getElementById('app')
);
