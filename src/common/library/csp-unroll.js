
const ContentSecurityPolicyDatabase = [
    { v:1, d:'default-src', c:'fetch' },
    { v:1, d:'img-src', c:'fetch' },
    { v:1, d:'style-src', c:'fetch' },
    { v:3, d:'style-src-attr', c:'fetch' },
    { v:3, d:'style-src-elem', c:'fetch' },
    { v:1, d:'font-src', c:'fetch' },
    { v:1, d:'media-src', c:'fetch' },
    { v:1, d:'object-src', c:'fetch' },
    { v:1, d:'script-src', c:'fetch' },
    { v:3, d:'script-src-attr', c:'fetch' },
    { v:3, d:'script-src-elem', c:'fetch' },
    { v:1, d:'connect-src', c:'fetch' },
    { v:1, d:'manifest-src', c:'fetch' },
    { v:1, d:'frame-src', c:'fetch' },
    { v:2, d:'child-src', c:'fetch' },
    { v:3, d:'prefetch-src', c:'fetch' },
    { v:2, d:'worker-src', c:'fetch' },
    { v:2, d:'base-uri', c:'document' },
    { v:3, d:'plugin-types', c:'document' },
    { v:1, d:'sandbox', c:'document' },
    { v:2, d:'disown-opener', c:'document' },
    { v:2, d:'form-action', c:'navigation' },
    { v:2, d:'frame-ancestors', c:'navigation' },
    { v:2, d:'navigate-to', c:'navigation' },
    { v:1, d:'report-uri', c:'reporting' },
    { v:2, d:'report-to', c:'reporting' },
    { v:2, d:'block-all-mixed-content', c:'other' },
    { v:2, d:'referrer', c:'other' },
    { v:2, d:'require-sri-for', c:'other' },
    { v:2, d:'upgrade-insecure-requests', c:'other' }
];

export function getKnownCspFields(version = 3) {
    return ContentSecurityPolicyDatabase
        .filter(csp => csp.v <= version)
        .map(csp => csp.d);
}


const CSP_FIELDS = getKnownCspFields(3);


export function usingCspOrderedFields(a,b) {
    return CSP_FIELDS.indexOf(a) - CSP_FIELDS.indexOf(b);
}

export function detectPolicyRevision(directiveList) {
    const matched = ContentSecurityPolicyDatabase
        .filter(csp => directiveList.indexOf(csp.d) > -1)
        .map(csp => csp.v);

    return Math.max.apply(null, matched);
}


const policyLead = [
    "*",
    "'none'",
    "'self'",
    "data:",
    "tel:",
    "mailto:",
    "https:",
];

const policyTail = [
    "'unsafe-inline'",
    "'unsafe-eval'"
];

export function usingDomainRight(a,b) {

    if(policyLead.includes(a)) return -1;
    else if(policyLead.includes(b)) return 1;
    
    if(policyTail.includes(b)) return -1;
    else if(policyTail.includes(a)) return 1;
    
    let ar = a.split('.').reverse().join('.');
    let br = b.split('.').reverse().join('.');

    if(ar < br) return -1;
    if(ar > br) return 1;

    return 0;
}


/**
 * parseCspHeader
 * @param {String} cspText - the "content" portion of the header
 * @returns {Object}
 */
export function parseCspHeader(cspText) {
    if(!cspText || typeof cspText !== 'string')
        return;

    return cspText.split(';')
        .map(x => x.trim())
        .map(b => b.split(' '))
        .reduce( (r,n) => {
            const directive = n[0];
            const targetDomain = n.slice(1);
            if(directive) r[directive] = targetDomain.sort(usingDomainRight);
            return r;
        },
    {});
}


/**
 * compileCspHeader
 * @param {Object} policyMap - dictionary of all properties
 * @returns {String}
 */
export function compileCspHeader(policyMap, reportOnly = false) {
    if(!policyMap) return '';

    const body = Object.keys(policyMap)
        .sort(usingCspOrderedFields)
        .map(d => `${d} ${policyMap[d].join(' ')}`)
        .join('; ');

    const headerText = reportOnly ? 'Content-Security-Policy-Report-Only' : 'Content-Security-Policy';
    return `${headerText}: ${body};`;
}


export function compareCsp(a,b) {

    const diff = {
        missing: [],
        unique: [],
    };
    
    const joined = Object.keys(a).concat(Object.keys(b));

    joined.forEach(jk => {
        const ak = a.hasOwnProperty(jk);
        const bk = b.hasOwnProperty(jk);
        if(ak && bk) return; // or 'shared'

        if(bk) diff.missing.push(jk);
        else if(ak) diff.unique.push(jk);
    });

    return diff;
}
