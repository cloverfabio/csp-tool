import request from 'request';
import { parseCspHeader } from './csp-unroll';


export function retrieveCspHeaderForUrl(urlTarget, cb) {
    request(urlTarget, {method: 'HEAD'}, function (error, response, body) {
         if(error) return cb(error, null);
         if(!response) return cb(true, null);
 
         const { statusCode, headers } = response;
         const cspLiveMode   = headers['content-security-policy'];
         const cspReportMode = headers['content-security-policy-report-only'];
 
         if(cspLiveMode && cspReportMode) {
             console.warn('warning... site has both live and reporting headers!');
         }
 
         let cspDictionary;
 
         if(cspLiveMode) {
             cspDictionary = parseCspHeader(cspLiveMode);
             console.log('\nCSP (live mode)', cspDictionary);
             return cb(false, cspDictionary, false); // live mode
         }
 
         cspDictionary = parseCspHeader(cspReportMode);
         console.log('\nCSP (reporting mode)', cspDictionary);
         return cb(false, cspDictionary, true); // report mode
     });
 }
