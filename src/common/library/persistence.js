import { remote } from 'electron';

import fs from 'fs';
import path from 'path';

import pkg from '../../../package.json';

const { app } = remote;

const STORAGE_PATH_NAME = 'persistence';

const SETTINGS_FILE_NAME = 'settings.json';
const BOOKMARKS_FILE_NAME = 'bookmarks.json';


/* more info here: https://github.com/electron/electron/blob/master/docs/api/app.md */

function throwError(err) { if(err) throw err; return; }

function getAppDataPath() {
    if(getAppDataPath._path) return getAppDataPath._path;

    const targetPath = path.join(app.getPath('appData'), pkg.name, STORAGE_PATH_NAME);
    try {
        fs.mkdirSync(targetPath);
    }
    catch(err) {
        if(err.code !== "EEXIST") throw err;
    }
    return getAppDataPath._path = targetPath;
}


export function loadBookmarks(onFinished) {
    fs.readFile(
        path.join( getAppDataPath(), BOOKMARKS_FILE_NAME),
        function fsReadFileDone(err, data) {
            if(err) throw err;
            try {
                const bookmarks = JSON.parse(data);
                onFinished(bookmarks);
            }
            catch(ex) {
                throw new Error('Failed parsing bookmarks.');
            }
        }
    );
}

export function saveBookmarks(bookmarks, onFinished) {
    fs.writeFile(
        path.join( getAppDataPath(), BOOKMARKS_FILE_NAME),
        JSON.stringify(bookmarks),
        { encoding: 'utf8' },
        onFinished || throwError
    );
}