
import React from 'react'
import { Icon, Label, Table } from 'semantic-ui-react'

import {
    detectPolicyRevision,
    getKnownCspFields,
    usingCspOrderedFields,
    usingDomainRight }
from 'common/library/csp-unroll';


function getPolicyDocumentNameFromRevision(ver) {
    const docName = 'Content Security Policy';
    switch(ver) {
        case 3: return `${docName} Level 3`;
        case 2: return `${docName} Level 2`;
        case 1: return `${docName} 1.0`;
        default: return `Unknown ${docName}`;
    }
}


export function AutoResponsiveTable(props) {
    if(!props.directives) return null;

    const theDirectives = Object.keys(props.directives);
    if(!theDirectives.length)
        return (<p style={{padding: '0.5em 0'}}>No content security policy directives found on this URL</p>);

    const cspFields = getKnownCspFields();

    const policyRevisionLevel = detectPolicyRevision(theDirectives);
    console.log('detected level:', policyRevisionLevel);

    return (
        <>

        <h3>{getPolicyDocumentNameFromRevision(policyRevisionLevel)}</h3>

        <Table unstackable celled size='small'>
            <Table.Header>
            <Table.Row>
                <Table.HeaderCell width="three">Directive</Table.HeaderCell>
                <Table.HeaderCell>Policy</Table.HeaderCell>
            </Table.Row>
            </Table.Header>
        
            <Table.Body>
            {
                theDirectives
                .sort(usingCspOrderedFields)
                .map(directiveName => {
                    const policies = Array.from(props.directives[directiveName]);
                    let irregular = false;
                    const decoration = {};
                    if(!cspFields.includes(directiveName)) {
                        decoration.negative = true;
                        irregular = true;
                    }
                    return (
                        <Table.Row key={directiveName} {...decoration}>
                            <Table.Cell>
                                {irregular && <Icon name="attention" />}
                                {directiveName}
                            </Table.Cell>
                            <Table.Cell>
                                {policies
                                    .sort(usingDomainRight)
                                    .map(p => {
                                        return (<Label key={p} basic size="small">{p}</Label>);
                                    })
                                }
                            </Table.Cell>
                        </Table.Row>
                    );
                }
            )}
            </Table.Body>
        </Table>
        </>
    );
}
