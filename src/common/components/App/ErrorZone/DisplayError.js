import React from 'react';

import { Message } from 'semantic-ui-react'

const MESSAGE_DISPLAY_MS = 9000;

export class DisplayError extends React.Component {

    constructor(props) {
        super(props);
        this.state = { visible: true };

        this.handleDismiss = this.handleDismiss.bind(this);

        const autoDismiss = () => this.setState({ visible: false });
        setTimeout(autoDismiss, MESSAGE_DISPLAY_MS);
    }

    handleDismiss() {
        this.setState({ visible: false })
    }

    render() {
        if (!this.state.visible) return null; // hidden

        const { error } = this.props;
        const { message } = error;
        // const { name, type, timeStamp, lineno, colno } = error;

        const userMessage = message.replace(/^uncaught.*?error:\s+/gi, '');
    
        return (
            <Message floating onDismiss={this.handleDismiss}>
                <Message.Header>{userMessage}</Message.Header>
                {/* <i>@{(timeStamp).toFixed(2)}ms - {lineno}:{colno}</i> */}
            </Message>
        );
    }
}

