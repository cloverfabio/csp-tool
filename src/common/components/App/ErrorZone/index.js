import React from 'react';
import { DisplayError } from './DisplayError';


export default class ErrorZone extends React.Component {

    constructor(props) {
        super(props);
        this.state = { errors: [] };
        this.receiveError = this.receiveError.bind(this);
    }

    componentDidMount() {
        window.addEventListener('error', this.receiveError, false);
    }

    // componentDidUpdate() { }

    componentWillUnmount() {
        window.removeEventListener('error', this.receiveError, false);
    }

    receiveError(error) {
        const errors = this.state.errors.concat(error);
        this.setState({ errors });
    }
    
    render() {
        return (
            <div className="error-zone">
                {this.state.errors.map(error => <DisplayError key={error.timeStamp} error={error} />)}
            </div>
        );
    }

}
