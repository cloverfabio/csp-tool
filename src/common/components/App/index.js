import React from 'react';

import ErrorZone from './ErrorZone';

import CspMain from 'common/components/CspMain'


export default function App() {
    return (
        <section>
            <ErrorZone />
            <CspMain />
        </section>
    );
}
