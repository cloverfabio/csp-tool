import React from 'react'
import { Icon, Label, Menu, Table } from 'semantic-ui-react'

import './styles.css'


function urlToElem(url) {
    const elem = document.createElement('a');
    elem.setAttribute('href', url);
    return elem;
}

function bookmarkType(elem) {
    return {
        href: elem.href,
        pathname: elem.pathname,
        host: elem.host,
        decorations: {}
    };
}


export function UrlCollection(props) {

    function decorateMatch(bookmark) {
        if(props.search && bookmark.href.indexOf(props.search) > -1) {
            bookmark.decorations.color = 'grey';
            if(bookmark.href === props.search) {
                bookmark.decorations.color = 'blue';
            }
        }
        else bookmark.decorations.basic = true;
        return bookmark;
    }
    
    const fixedList = []; // sample

    const comboList = [].concat(fixedList).concat(props.bookmarks)
                        .map(urlToElem).map(bookmarkType)
                        .map(decorateMatch);
    return (
        <div className="url-collection panel-min-height">
            <div className="url-collection--panel panel-margin-width">
                Bookmarks
            </div>
            <div className="url-collection--main panel-margin-offset">
                <Label.Group> {
                    comboList.map(entry => 
                        <Label
                            key={entry.href} {...entry.decorations}
                            onMouseDown={e => props.onClickUrl(entry.href)}
                        >
                            {entry.host}
                            <Label.Detail>{entry.pathname}</Label.Detail>
                            <Icon name='delete'
                                onMouseDown={e => e.stopPropagation()}
                                onClick={e => props.onClickUrlRemove(entry.href)}
                            />
                        </Label>)
                }
                </Label.Group>
            </div>
        </div>
    );
}
