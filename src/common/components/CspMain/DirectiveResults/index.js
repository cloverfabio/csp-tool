import React from 'react';

import { AutoResponsiveTable } from 'common/components/AutoResponsiveTable';

import './styles.css'

export class DirectiveResults extends React.Component {
    render() {
        return (
            <div className="directive-results panel-min-height">
                <div className="directive-results--panel panel-margin-width">
                    Directives
                </div>
                <div className="directive-results--main panel-margin-offset">
                    <AutoResponsiveTable directives={this.props.results} />
                </div>
            </div>
        )
    }
}
