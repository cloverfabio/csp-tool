import React from 'react';

import { Input, Button } from 'semantic-ui-react'

import './styles.css'

export class UrlEntry extends React.Component {
    render() {
        return (
            <form className="url-entry panel-min-height" onSubmit={this.props.onSubmit}>
                <div className="url-entry--panel panel-margin-width">
                    URL
                </div>
                <div className="url-entry--main panel-margin-offset">
                    <Input fluid icon='linkify'
                        disabled={this.props.loadingUrl}
                        placeholder="URL"
                        value={this.props.value}
                        onChange={this.props.onChange}
                    />
                </div>
                <div className="url-entry--go">
                    <Button loading={this.props.loadingUrl} onClick={this.props.onConfirm}>Go</Button>
                </div>
            </form>
        )
    }
}
