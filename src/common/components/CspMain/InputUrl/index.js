import React from 'react';

import { Input, Button } from 'semantic-ui-react'

import './styles.css'

export class InputUrl extends React.Component {
    render() {
        return (
            <div className="input-url">
                <div className="input-url--panel">
                    Panel
                </div>
                <div className="input-url--main">
                    <Input fluid value={this.props.value} onChange={this.props.onChange} />
                </div>
                <div className="input-url--go">
                    <Button onClick={this.props.onConfirm}>Go</Button>
                </div>
            </div>
        )
    }
}
