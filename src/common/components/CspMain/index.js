import React from 'react'

import { retrieveCspHeaderForUrl } from 'common/library/csp-fetch';
import { loadBookmarks, saveBookmarks } from 'common/library/persistence';

import { UrlEntry } from './UrlEntry';
import { UrlCollection } from './UrlCollection';
import { DirectiveResults } from './DirectiveResults';


const HTTP_SCHEME = /^https?:\/\//;
const OTHER_SCHEME = /^[a-z]+:\/\//;

export default class CspMain extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            targetUrl: '',
            bookmarks: [],
            directives: null
        };
        this.addToBookmarks = this.addToBookmarks.bind(this);
        this.removeFromBookmarks = this.removeFromBookmarks.bind(this);
        this.setUrl = this.setUrl.bind(this);
        this.updateUrl = this.updateUrl.bind(this);
        this.detectEnter = this.detectEnter.bind(this);
        this.checkTargetUrl = this.checkTargetUrl.bind(this);
        this.inspectTargetUrl = this.inspectTargetUrl.bind(this);
        
        loadBookmarks(bookmarks => {
            if(Array.isArray(bookmarks) && bookmarks.length) this.setState({bookmarks});
        });
    }

    addToBookmarks(url) {
        if(this.state.bookmarks.includes(url)) return;

        const bookmarks = [].concat(this.state.bookmarks)
        bookmarks.push(url)

        this.setState({bookmarks});
        saveBookmarks(bookmarks);
    }

    removeFromBookmarks(url) {
        const bidx = this.state.bookmarks.indexOf(url);
        if(bidx < 0) return;

        const bookmarks = []
            .concat(this.state.bookmarks.slice(0,bidx))
            .concat(this.state.bookmarks.slice(bidx+1));

        this.setState({bookmarks});
        saveBookmarks(bookmarks);
    }

    setUrl(targetUrl) {
        this.setState({ targetUrl, directives: null });
    }

    updateUrl(e) {
        this.setState({ targetUrl: e.currentTarget.value, directives: null });
    }

    detectEnter(e) {
        e.preventDefault(); // we are not submitting
        if(e.charCode === 13) return this.checkTargetUrl();
    }

    checkTargetUrl() {
        let baseUrl = this.state.targetUrl.trim();
        if(baseUrl.length < 4) return;

        let scheme;
        if(scheme = baseUrl.match(HTTP_SCHEME)) {
            this.inspectTargetUrl();
            return;
        }
        else if(scheme = baseUrl.match(OTHER_SCHEME)) {
            throw new Error('Unsupported protocol: ' + scheme[0]  );
        }

        this.setState({targetUrl: 'https://' + baseUrl }, this.inspectTargetUrl);
    }

    inspectTargetUrl() {
        this.setState({loading: true});

        const thenSetDirectives = (err, directives) => {
            if(err) {
                this.setState({loading: false, directives: null});
                throw err;
            }

            const reDirectives = directives || {};

            this.addToBookmarks(this.state.targetUrl);
            this.setState({loading: false, directives: reDirectives});
        }

        retrieveCspHeaderForUrl(this.state.targetUrl, thenSetDirectives);
    }

    render() {
        return (
            <div className="csp-main">
                <UrlCollection
                    bookmarks={this.state.bookmarks}
                    search={this.state.targetUrl}
                    onClickUrl={this.setUrl}
                    onClickUrlRemove={this.removeFromBookmarks}
                />
                <UrlEntry
                    loadingUrl={this.state.loading}
                    value={this.state.targetUrl}
                    onChange={this.updateUrl}
                    onSubmit={this.detectEnter}
                    onConfirm={this.checkTargetUrl}
                />
                <DirectiveResults
                    results={this.state.directives}
                    active={this.state.targetUrl}
                />
            </div>
        );
    }
}
