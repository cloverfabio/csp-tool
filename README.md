# CSP Tool

A tool to manage [`Content Security Policy`](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) declarations.


### Getting Started

Built with [`Electron-Webpack`](./electron-webpack.md)

* Clone
* Install dependencies: `yarn`
* Run `yarn dev` ~~or build `yarn dist`~~


```bash
# clone the project
git clone https://cloverfabio@bitbucket.org/cloverfabio/csp-tool.git

# change working directory
cd csp-tool

# install dependencies
yarn
```

### Development Scripts

```bash
# run application in development mode
yarn dev

# compile source code and create webpack output
yarn compile

# `yarn compile` & create build with electron-builder
yarn dist

# `yarn compile` & create unpacked build with electron-builder
yarn dist:dir
```
